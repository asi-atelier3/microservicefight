package com.sp.repository;
import java.util.List;

import model.Family;
import org.springframework.data.repository.CrudRepository;

import com.sp.model.Fight;

public interface FightRepository extends CrudRepository<Fight, Long> {

	public List<Fight> findAll();

	public Fight findById(long id);

	public Fight save(Fight fight);

	public void delete(Fight fight);
}