package com.sp.model;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import model.Family;

@Entity
public class Fight {    
	@Id
    private long id;
    private long p1Energy, p1Hp, p2Energy, p2Hp;

	public Fight() {}

    public Fight(long id, long p1Energy, long p1Hp, long p2Energy, long p2Hp) {
        this.id = id;
        this.p1Energy = p1Energy;
        this.p1Hp = p1Hp;
        this.p2Energy = p2Energy;
        this.p2Hp = p2Hp;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getP1Energy() {
        return this.p1Energy;
    }

    public void setP1Energy(long p1Energy) {
        this.p1Energy = p1Energy;
    }

    public long getP1Hp() {
        return this.p1Hp;
    }

    public void setP1Hp(long p1Hp) {
        this.p1Hp = p1Hp;
    }

    public long getP2Energy() {
        return this.p2Energy;
    }

    public void setP2Energy(long p2Energy) {
        this.p2Energy = p2Energy;
    }

    public long getP2Hp() {
        return this.p2Hp;
    }

    public void setP2Hp(long p2Hp) {
        this.p2Hp = p2Hp;
    }

    @Override
    public String toString() {
        return "{" +
            "id='" + getId() + "'" +
            ", p1Energy='" + getP1Energy() + "'" +
            ", p1Hp='" + getP1Hp() + "'" +
            ", p2Energy='" + getP2Energy() + "'" +
            ", p2Hp='" + getP2Hp() + "'" +
            "}";
    }

}
