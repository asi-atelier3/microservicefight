package com.sp.service;


import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sp.model.ApiURL;
import com.sp.model.Fight;
import com.sp.repository.FightRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import model.CardDTO;
import model.RoomDTO;

@Service
public class FightService {
	@Autowired
	FightRepository fightRepository;

	public ArrayList<Fight> getFights() {
		ArrayList<Fight> fights = new ArrayList<>();
		Iterable<Fight> itFight = fightRepository.findAll();
		for (Fight fight : itFight) {
			fights.add(fight);
		}
		return fights;
	}

	public boolean addFight(Fight fight) {
		try {
			fightRepository.save(fight);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public Fight attack(long id, long player) throws IOException, InterruptedException {
		Fight fight = fightRepository.findById(id);

		HttpRequest request = HttpRequest.newBuilder().uri(URI.create(ApiURL.RoomAPI + "/" + id)).header("Content-Type", "application/json").GET().build();
		HttpResponse response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
		if(response.statusCode() != 200) return null;
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		RoomDTO room = objectMapper.readValue(response.body().toString(), RoomDTO.class);

		request = HttpRequest.newBuilder().uri(URI.create(ApiURL.CardAPI + "/" + room.getcreatorCardId())).header("Content-Type", "application/json").GET().build();
		response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
		if(response.statusCode() != 200) return null;
		objectMapper = new ObjectMapper();
		objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		CardDTO card1 = objectMapper.readValue(response.body().toString(), CardDTO.class);

		request = HttpRequest.newBuilder().uri(URI.create(ApiURL.CardAPI + "/" + room.getAdversaryCardId())).header("Content-Type", "application/json").GET().build();
		response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
		if(response.statusCode() != 200) return null;
		objectMapper = new ObjectMapper();
		objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		CardDTO card2 = objectMapper.readValue(response.body().toString(), CardDTO.class);

		if (player == 1) {
			// decrese energy
			if (fight.getP1Energy() <= 0) {
				fight.setP1Energy(10);
			} else {
				fight.setP1Energy(fight.getP1Energy() - 10);
				// prevent the negative energy 
				if (fight.getP1Energy() < 0) {
					fight.setP1Energy(0);
				}
				// compute a random damage to each player with the attack and the defense
				int damageCard = (int) (Math.random() * card1.getAttack() - Math.random() * card2.getDefense());
				// if the damage is negative, set it to 0
				if (damageCard < 0) damageCard = 0;
				// set the new hp of the cards and put it to 0 if its nefative
				fight.setP2Hp(fight.getP2Hp() - damageCard);
				if (fight.getP2Hp() < 0) {
					fight.setP2Hp(0);
				}
			}
		}
		else {
			// decrese energy
			if (fight.getP2Energy() <= 0) {
				fight.setP2Energy(10);
			} else {
				fight.setP2Energy(fight.getP2Energy() - 10);
				// compute a random damage to each player with the attack and the defense
				int damageCard = (int) ((Math.random() * card2.getAttack() - Math.random() * card1.getDefense())*0.5);
				// if the damage is negative, set it to 0
				if (damageCard < 0) damageCard = 0;
				// set the new hp of the cards and put it to 0 if its nefative
				fight.setP1Hp(fight.getP1Hp() - damageCard);
				if (fight.getP1Hp() < 0) {
					fight.setP1Hp(0);
				}
			}
		}
		// update fight and return it
		fightRepository.save(fight);
		return fight;
	}
}