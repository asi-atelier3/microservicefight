package com.sp.rest;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import model.CardDTO;

import com.sp.model.Fight;
import com.sp.service.FightService;

@CrossOrigin
@RestController
@RequestMapping("/api/fights")
public class FightRestCtr {
    @Autowired
    FightService fightService;
    /*
     * GET /fights
     */
    @RequestMapping(method = RequestMethod.GET, value = "")
    public ArrayList<Fight> getFights() {
        ArrayList<Fight> fights = fightService.getFights();
        return fights;
    }

    /*
     * POST /fights
     */
    @RequestMapping(method = RequestMethod.POST, value = "")
    public boolean addFight(@RequestBody Fight fight) {
        return fightService.addFight(fight);
    }

    /*
     * PUT /cards/:id
     */
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public Fight attack(@PathVariable long id, @RequestParam() long player) throws IOException, InterruptedException {
        return fightService.attack(id, player);
    }

}
