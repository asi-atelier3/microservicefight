# Run
FROM openjdk:11-jre
ENV FIGHTSERVICE_HOME /opt/fightservice
WORKDIR $FIGHTSERVICE_HOME
COPY ./target/*.jar $FIGHTSERVICE_HOME/fightservice.jar

EXPOSE 8084

ENTRYPOINT java -jar fightservice.jar